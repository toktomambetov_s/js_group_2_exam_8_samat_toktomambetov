import React, { Component, Fragment } from 'react';
import Navbar from "./components/Navbar/Navbar";
import {Switch, Route} from "react-router-dom";
import AddQuoteForm from "./containers/AddQuoteForm/AddQuoteForm";
import QuoteList from "./containers/QuoteList/QuoteList";
import StarWars from "./containers/StarWars/StarWars";
import FamousPeople from "./containers/FamousPeople/FamousPeople";
import Saying from "./containers/Saying/Saying";
import Humour from "./containers/Humour/Humour";
import Motivational from "./containers/Motivational/Motivational";

class App extends Component {
  render() {
    return (
        <Fragment>
            <Navbar />

            <Switch>
                <Route path="/" exact component={QuoteList}/>
                <Route path="/add-quote" component={AddQuoteForm}/>
                <Route path="/star-wars" component={StarWars}/>
                <Route path="/famous-people" component={FamousPeople}/>
                <Route path="/saying" component={Saying}/>
                <Route path="/humour" component={Humour}/>
                <Route path="/motivational" component={Motivational}/>
            </Switch>
        </Fragment>
    );
  }
}

export default App;
