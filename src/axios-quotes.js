import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://exam-8-20d56.firebaseio.com/'
});

export default instance;