import React from 'react';
import './Form.css';

const Form = props => {
    return (
        <div>
            <form className="Form">
                <label className="mr-sm-2" for="inlineFormCustomSelect">Category</label>
                <select onChange={props.category} className="custom-select mb-2 mr-sm-2 mb-sm-0" id="inlineFormCustomSelect">
                    <option selected>Choose...</option>
                    <option>Star Wars</option>
                    <option>Famous people</option>
                    <option>Saying</option>
                    <option>Humour</option>
                    <option>Motivational</option>
                </select>
                <div className="form-group">
                    <label>Author</label>
                    <input onChange={props.author} type="text" className="form-control" id="exampleFormControlInput1" placeholder="Author"/>
                </div>
                <div className="form-group">
                    <label>Quote text</label>
                    <textarea onChange={props.quoteText} className="form-control" id="exampleFormControlTextarea1" rows="3"/>
                </div>
                <button onClick={props.click} type="button"  className="btn btn-primary">Save</button>
            </form>
        </div>
    );
};

export default Form;