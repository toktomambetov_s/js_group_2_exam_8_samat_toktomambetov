import React from 'react';
import {NavLink} from 'react-router-dom';

const Navbar = () => {
    return (
        <nav className="navbar navbar-toggleable-md navbar-light bg-faded">
            <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"/>
            </button>
            <a className="navbar-brand" href="#">Quotes Central</a>
            <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="navbar-nav">
                    <li className="nav-item">
                            <NavLink className="nav-link" to="/">Quotes<span className="sr-only">(current)</span></NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/add-quote">Submit new quote</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/star-wars">Star Wars</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/famous-people">Famous people</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/saying">Saying</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/humour">Humour</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/motivational">Motivational</NavLink>
                    </li>
                </ul>
            </div>
        </nav>
    );
};

export default Navbar;