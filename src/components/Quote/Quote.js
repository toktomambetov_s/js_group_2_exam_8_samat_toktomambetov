import React from 'react';
import './Quote.css';

const Quote = props => {
    return (
        <div className="Quote">
            <p className="Text">"{props.text}"<span className="Author"> - {props.author}</span></p>
            <div className="Buttons">
                <button className="EditBtn">Edit</button>
                <button className="DeleteBtn" onClick={props.click}>Delete</button>
            </div>
        </div>
    );
};

export default Quote;