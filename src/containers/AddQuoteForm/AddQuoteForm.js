import React, { Component, Fragment } from 'react';
import Form from "../../components/Form/Form";

import axios from '../../axios-quotes';

class AddQuoteForm extends Component {
    state = {
        category: '',
        author: '',
        text: '',
    };

    changeCategory = e => this.setState({category: e.target.value});
    changeAuthorName = e => this.setState({author: e.target.value});
    changeQuoteText = e => this.setState({text: e.target.value});

    addQuote = event => {
        event.preventDefault();
        const quote = {
          category: this.state.category,
          author: this.state.author,
          text: this.state.text
        };

        axios.post('/quotes.json', quote).finally(() => {
            this.props.history.push('/');
        })
    };

    render() {
        return (
            <Fragment>
                <h1>Submit new quote</h1>
                <Form category={this.changeCategory} author={this.changeAuthorName} quoteText={this.changeQuoteText} click={this.addQuote}/>
            </Fragment>
        );
    }
}

export default AddQuoteForm;