import React, { Component, Fragment } from 'react';

import axios from '../../axios-quotes';
import Quote from "../../components/Quote/Quote";

class Humour extends Component {
    state = {
        quotes: []
    };

    componentDidMount() {
        axios.get('quotes.json?orderBy="category"&equalTo="Humour"').then(response => {
            console.log(response);
            const quotes = [];
            for (let key in response.data) {
                quotes.push({...response.data[key], id: key});
            }

            this.setState({quotes: quotes});
        })
    };

    render() {
        return (
            <Fragment>
                <h1>Humour quotes</h1>
                <section className="Quotes">
                    {this.state.quotes.map(quote => (
                        <Quote author={quote.author}
                               text={quote.text}
                        />
                    ))}
                </section>
            </Fragment>
        )
    };
}

export default Humour;

