import React, { Component, Fragment } from 'react';

import axios from '../../axios-quotes';
import Quote from "../../components/Quote/Quote";

class QuoteList extends Component {
    state = {
        quotes: [],
    };

    componentDidMount() {
        axios.get('quotes.json').then(response => {
            console.log(response);
            const quotes = [];
            for (let key in response.data) {
                quotes.push({...response.data[key], id: key});
            }

            this.setState({quotes: quotes});
        })
    }

    deleteQuote = (id, index) => {
        console.log(id);
        axios.delete('/quotes/' + id + '.json').finally(() => {
            this.props.history.push('/');
        });

        const quotes = [...this.state.quotes];
        quotes.splice(index, 1);
        this.setState({quotes});
    };

    render() {
        return (
            <Fragment>
                <section className="Quotes">
                    {this.state.quotes.map((quote, index) => (
                        <Quote author={quote.author}
                               text={quote.text}
                               click={() => this.deleteQuote(quote.id, index)}
                        />
                    ))}
                </section>
            </Fragment>
        );
    }
}

export default QuoteList;